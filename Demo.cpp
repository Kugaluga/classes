#include <iostream>
#include <bits/stdc++.h>
#include "Demo.h"

using namespace std;

 
 int main(){
	 Investment q;
	 float goal = 1000000.00;
	 int yrs = 0;
	 
	 cout << endl << "   How long will it take for Fred to become a Millionaire?" << endl << endl;
	 cout << "Initial Investment of $" << q.getInvstmnt() << endl;
	 cout << "Monthly Additional Investments of $" << q.getMonRate() << endl;
	 cout << "Annual Interest Rate of 8.3%" << endl;
	 
	 while(q.getVal() < goal){
		 q.setVal();
		 yrs++;
	 }
	 
	 cout <<endl << "It would take Fred " << yrs << " years to become a Millionaire." << endl;
 }
