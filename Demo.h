class Investment 
 {
    private:  //properties
         long double val;
		 long double invstmnt;
		 long double mon_intrst_rate;
		 float intrst_rate;
		 int n;
         


    public:  //methods
    
		Investment(){
			val = 100.00;
			invstmnt = 100.00;
			intrst_rate = 0.083;
			mon_intrst_rate = 250.00;
			n = 12;
		}
	
		void setVal(){
			val = val + (mon_intrst_rate * n);
			val = val + (val * intrst_rate);
		}
			
		long double getVal(){
			return val;
		}
			
		long double getInvstmnt(){
			return invstmnt;
		}
		
		long double getMonRate(){
			return mon_intrst_rate;
		}
		
 };

